/****** for fina2bcc database     Script Date: 07/10/2009 11:51:35, for database : fina2bcc20090619******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IN_RETURN_ITEMS]') AND name = N'return_complex2')
DROP INDEX [return_complex2] ON [dbo].[IN_RETURN_ITEMS] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IN_RETURN_ITEMS]') AND name = N'return_items_id')
DROP INDEX [return_items_id] ON [dbo].[IN_RETURN_ITEMS] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IN_RETURN_ITEMS]') AND name = N'return_items_nodeID')
DROP INDEX [return_items_nodeID] ON [dbo].[IN_RETURN_ITEMS] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IN_RETURN_ITEMS]') AND name = N'return_items_returnID')
DROP INDEX [return_items_returnID] ON [dbo].[IN_RETURN_ITEMS] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IN_RETURN_ITEMS]') AND name = N'return_items_rowNumber')
DROP INDEX [return_items_rowNumber] ON [dbo].[IN_RETURN_ITEMS] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IN_RETURN_ITEMS]') AND name = N'return_items_tableID')
DROP INDEX [return_items_tableID] ON [dbo].[IN_RETURN_ITEMS] WITH ( ONLINE = OFF )

CREATE NONCLUSTERED INDEX [complex_returns_index] ON [dbo].[IN_RETURN_ITEMS] 
(
	[RETURNID] ASC,
	[TABLEID] ASC,
	[NODEID] ASC,
	[VERSIONID] ASC,
	[ROWNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]