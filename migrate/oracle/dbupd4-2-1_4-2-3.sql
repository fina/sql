/*
 Database: Oracle 11g
 
 Author: Nick Gochiashvili
 E: nick@fina2.net
 Version: 0.1
 Date : 15/11/2012
*/

/*
  Update DB Version
*/
update sys_properties set value='4.2.3' where prop_key='fina2.database.schemaVersion';

/*
	Add OUT_REPORTS code and optlock columns
*/
ALTER TABLE OUT_REPORTS ADD code varchar2(12) NULL;
ALTER TABLE OUT_REPORTS ADD optlock INT DEFAULT 0 NOT NULL;

/*
  Add IN_MANAGING_BODIES code column
 */
ALTER TABLE IN_MANAGING_BODIES ADD code varchar2 (12) NULL;

/**
Add Properties
 */

DELETE FROM SYS_PROPERTIES WHERE RTRIM(PROP_KEY) IN('fina2.process.unchangedReturn.statuses','fina2.auditlog.enable');
INSERT INTO SYS_PROPERTIES(PROP_KEY,VALUE) VALUES('fina2.process.unchangedReturn.statuses','');
INSERT INTO SYS_PROPERTIES(PROP_KEY,VALUE) VALUES('fina2.auditlog.enable','1');

ALTER TABLE SYS_AUDIT_LOG drop CONSTRAINT PK_AUDIT_TRAIL;

commit;

declare
  sysPermId   SYS_PERMISSIONS.ID%TYPE;
  sysStringId SYS_STRINGS.Id%TYPE;
  langId      SYS_LANGUAGES.ID%TYPE;
  permCounter SYS_PERMISSIONS.Id%TYPE;
begin
  select max(id) + 1 into sysPermId from sys_permissions;
  select max(id) + 1 into sysStringId from sys_strings;
  select id
    into langId
    from SYS_LANGUAGES
   where lower(ltrim(rtrim(code))) = 'en_us'
      or lower(ltrim(rtrim(code))) = 'en' and rownum<2;
 
    dbms_output.put_line('Inserting audit trail log');
    insert into sys_Permissions
      (id, Idname, Namestrid)
    values
      (sysPermId, 'net.fina.auditTrailLog', sysStringId);
    insert into sys_strings
      (id, langid, value)
    values
      (sysStringId, langId, 'Audit trail log');
    commit;
    dbms_output.put_line('Permission inserted');

end;