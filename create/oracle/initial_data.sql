insert into sys_languages (id, code, name, dateformat, numberformat, fontface, fontsize, htmlcharset, xmlencoding) values (1, 'en_US', 'English', 'dd/MM/yyyy', '#0.00', 'Arial', 12, 'ASCII', 'ASCII'); 
insert into sys_strings (id, langid, value) values (1, 1, 'admin');
insert into sys_strings (id, langid, value) values (2, 1, 'System Administrator');
insert into sys_strings (id, langid, value) values (3, 1, 'File');
insert into sys_strings (id, langid, value) values (4, 1, 'Menu Tree');
insert into sys_strings (id, langid, value) values (5, 1, 'Exit');
insert into sys_strings (id, langid, value) values (6, 1, 'Languages');
insert into sys_strings (id, langid, value) values (7, 1, 'User Manager');
insert into sys_strings (id, langid, value) values (8, 1, 'Banks');
insert into sys_strings (id, langid, value) values (9, 1, 'License Types');
insert into sys_strings (id, langid, value) values (10, 1, 'Bank Types');
insert into sys_strings (id, langid, value) values (11, 1, 'Peer Groups');
insert into sys_strings (id, langid, value) values (12, 1, 'Bank List');
insert into sys_strings (id, langid, value) values (13, 1, 'Meta Data');
insert into sys_strings (id, langid, value) values (14, 1, 'Meta Data Tree');
insert into sys_strings (id, langid, value) values (15, 1, 'Comparison Rules');
insert into sys_strings (id, langid, value) values (16, 1, 'Period Types');
insert into sys_strings (id, langid, value) values (17, 1, 'Period Definition');
insert into sys_strings (id, langid, value) values (18, 1, 'Period Auto Insert');
insert into sys_strings (id, langid, value) values (19, 1, 'Return Types');
insert into sys_strings (id, langid, value) values (20, 1, 'Return Definition');
insert into sys_strings (id, langid, value) values (21, 1, 'Schedule Definition');
insert into sys_strings (id, langid, value) values (22, 1, 'Schedule Auto Insert');
insert into sys_strings (id, langid, value) values (23, 1, 'Processing');
insert into sys_strings (id, langid, value) values (24, 1, 'Return Manager');
insert into sys_strings (id, langid, value) values (25, 1, 'Import');
insert into sys_strings (id, langid, value) values (26, 1, 'File Robot');
insert into sys_strings (id, langid, value) values (27, 1, 'Return Statuses');
insert into sys_strings (id, langid, value) values (28, 1, 'Reporting');
insert into sys_strings (id, langid, value) values (29, 1, 'Report Manager');
insert into sys_strings (id, langid, value) values (30, 1, 'Metadata Tree Amend');
insert into sys_strings (id, langid, value) values (31, 1, 'Metadata Tree Delete');
insert into sys_strings (id, langid, value) values (32, 1, 'Metadata Tree Review');
insert into sys_strings (id, langid, value) values (33, 1, 'Banks Amend');
insert into sys_strings (id, langid, value) values (34, 1, 'Banks Delete');
insert into sys_strings (id, langid, value) values (35, 1, 'Banks Review');
insert into sys_strings (id, langid, value) values (36, 1, 'Return Definitions Amend');
insert into sys_strings (id, langid, value) values (37, 1, 'Return Definitions Delete');
insert into sys_strings (id, langid, value) values (38, 1, 'Return Definitions Review');
insert into sys_strings (id, langid, value) values (39, 1, 'Return Definitions Format');
insert into sys_strings (id, langid, value) values (40, 1, 'Menu Amend');
insert into sys_strings (id, langid, value) values (41, 1, 'Users Amend');
insert into sys_strings (id, langid, value) values (42, 1, 'Schedules Amend');
insert into sys_strings (id, langid, value) values (43, 1, 'Schedules Delete');
insert into sys_strings (id, langid, value) values (44, 1, 'Schedules Review');
insert into sys_strings (id, langid, value) values (45, 1, 'Returns Amend');
insert into sys_strings (id, langid, value) values (46, 1, 'Returns Delete');
insert into sys_strings (id, langid, value) values (47, 1, 'Returns Review');
insert into sys_strings (id, langid, value) values (48, 1, 'Returns Process');
insert into sys_strings (id, langid, value) values (49, 1, 'Returns Accept');
insert into sys_strings (id, langid, value) values (50, 1, 'Returns Reset');
insert into sys_strings (id, langid, value) values (51, 1, 'Returns Reject');
insert into sys_strings (id, langid, value) values (52, 1, 'Periods Amend');
insert into sys_strings (id, langid, value) values (53, 1, 'Periods Delete');
insert into sys_strings (id, langid, value) values (54, 1, 'Periods Review');
insert into sys_strings (id, langid, value) values (55, 1, 'Reports Amend');
insert into sys_strings (id, langid, value) values (56, 1, 'Reports Generate');
insert into sys_strings (id, langid, value) values (57, 1, 'Return Statuses');
insert into sys_strings (id, langid, value) values (58, 1, 'Report Scheduler');
insert into sys_strings (id, langid, value) values (59, 1, 'Reports Scheduler Manager');
insert into sys_strings (id, langid, value) values (60, 1, 'Reports Schedules Add');
insert into sys_strings (id, langid, value) values (61, 1, 'Stored Reports Manager');
insert into sys_strings (id, langid, value) values (62, 1, 'Return Version Amend');
insert into sys_strings (id, langid, value) values (63, 1, 'Return Version Delete');
insert into sys_strings (id, langid, value) values (64, 1, 'Return Version Review');
insert into sys_strings (id, langid, value) values (65, 1, 'Return Versions');
insert into sys_users (id, login, password, namestrid, titlestrid, phone, email) values (1, 'sa', 'b8c6f827a3f7a6f15d5f971920f79dd20d712889',1 ,2, '<admin phone>', '<admin email>');
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (1, 0, 3, 0, '', '', 0);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (2, 1, 4, 1, 'fina2.actions.menuAmend', '', 1);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (3, 1, 5, 1, 'fina2.actions.exit', '', 4);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (4, 1, 6, 1, 'fina2.actions.languages', '', 3);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (5, 1, 7, 1, 'fina2.actions.users', '', 2);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (6, 0, 8, 0, '', '', 1);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (7, 6, 9, 1, 'fina2.actions.licences', '', 1);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (8, 6, 10, 1, 'fina2.actions.bankTypes', '', 2);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (9, 6, 11, 1, 'fina2.actions.bankGroups', '', 3);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (10, 6, 12, 1, 'fina2.actions.banks', '', 4);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (11, 0, 13, 0, '', '', 2);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (12, 11, 14, 1, 'fina2.actions.MDTAmend', '', 1);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (13, 11, 15, 1, 'fina2.actions.metadataComparisons', '', 2);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (14, 11, 16, 1, 'fina2.actions.periodTypes', '', 3);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (15, 11, 17, 1, 'fina2.actions.periods', '', 4);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (16, 11, 18, 1, 'fina2.period.periodAutoInsert', '', 5);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (17, 11, 19, 1, 'fina2.actions.returnTypes', '', 6);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (18, 11, 20, 1, 'fina2.actions.returnDefinitions', '', 7);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (28, 11, 65, 1, 'fina2.actions.returnVersions', '', 8);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (19, 11, 21, 1, 'fina2.actions.schedules', '', 9);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (20, 11, 22, 1, 'fina2.returns.autoSchedule', '', 10);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (21, 0, 23, 0, '', '', 3);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (22, 21, 24, 1, 'fina2.actions.returnManager', '', 1);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (23, 21, 25, 1, 'fina2.actions.import', '', 2);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (24, 21, 26, 1, 'fina2.actions.fileRobot', '', 3);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (25, 21, 27, 1, 'fina2.actions.returnsStatuses', '', 4);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (26, 0, 28, 0, '', '', 4);
insert into sys_menus (id, parentid, namestrid, type, actionkey, application, sequence) values (27, 26, 29, 1, 'fina2.actions.reportManager', '', 1);
insert into sys_permissions (id, namestrid, idname) values (1, 30, 'fina2.metadata.amend');
insert into sys_permissions (id, namestrid, idname) values (2, 31, 'fina2.metadata.delete');
insert into sys_permissions (id, namestrid, idname) values (3, 32, 'fina2.metadata.review');
insert into sys_permissions (id, namestrid, idname) values (4, 33, 'fina2.bank.amend');
insert into sys_permissions (id, namestrid, idname) values (5, 34, 'fina2.bank.delete');
insert into sys_permissions (id, namestrid, idname) values (6, 35, 'fina2.bank.review');
insert into sys_permissions (id, namestrid, idname) values (7, 36, 'fina2.returns.definition.amend');
insert into sys_permissions (id, namestrid, idname) values (8, 37, 'fina2.returns.definition.delete');
insert into sys_permissions (id, namestrid, idname) values (9, 38, 'fina2.returns.definition.review');
insert into sys_permissions (id, namestrid, idname) values (10, 39, 'fina2.returns.definition.format');
insert into sys_permissions (id, namestrid, idname) values (11, 40, 'fina2.menu.amend');
insert into sys_permissions (id, namestrid, idname) values (12, 41, 'fina2.security.amend');
insert into sys_permissions (id, namestrid, idname) values (13, 42, 'fina2.returns.schedule.amend');
insert into sys_permissions (id, namestrid, idname) values (14, 43, 'fina2.returns.schedule.delete');
insert into sys_permissions (id, namestrid, idname) values (15, 44, 'fina2.returns.schedule.review');
insert into sys_permissions (id, namestrid, idname) values (16, 45, 'fina2.returns.amend');
insert into sys_permissions (id, namestrid, idname) values (17, 46, 'fina2.returns.delete');
insert into sys_permissions (id, namestrid, idname) values (18, 47, 'fina2.returns.review');
insert into sys_permissions (id, namestrid, idname) values (19, 48, 'fina2.returns.process');
insert into sys_permissions (id, namestrid, idname) values (20, 49, 'fina2.returns.accept');
insert into sys_permissions (id, namestrid, idname) values (21, 50, 'fina2.returns.reset');
insert into sys_permissions (id, namestrid, idname) values (22, 51, 'fina2.returns.reject');
insert into sys_permissions (id, namestrid, idname) values (23, 52, 'fina2.periods.amend');
insert into sys_permissions (id, namestrid, idname) values (24, 53, 'fina2.periods.delete');
insert into sys_permissions (id, namestrid, idname) values (25, 54, 'fina2.periods.review');
insert into sys_permissions (id, namestrid, idname) values (26, 55, 'fina2.report.amend');
insert into sys_permissions (id, namestrid, idname) values (27, 56, 'fina2.report.generate');
insert into sys_permissions (id, namestrid, idname) values (28, 57, 'fina2.returns.statuses');
insert into sys_permissions (id, namestrid, idname) values (29, 58, 'fina2.report.scheduler');
insert into sys_permissions (id, namestrid, idname) values (30, 59, 'fina2.reports.scheduler.manager');
insert into sys_permissions (id, namestrid, idname) values (31, 60, 'fina2.reports.scheduler.add');
insert into sys_permissions (id, namestrid, idname) values (32, 61, 'fina2.reports.stored.manager');
insert into sys_permissions (id, namestrid, idname) values (33, 62, 'fina2.returns.version.amend');
insert into sys_permissions (id, namestrid, idname) values (34, 63, 'fina2.returns.version.delete');
insert into sys_permissions (id, namestrid, idname) values (35, 64, 'fina2.returns.version.review');
insert into sys_properties (prop_key, value) values ('fina2.security.allowedNumberLoginAttempt', '3');
insert into sys_properties (prop_key, value) values ('fina2.security.allowedAccountInactivityPerioed', '60');
insert into sys_properties (prop_key, value) values ('fina2.security.numberOfStoredOldPasswords', '13');
insert into sys_properties (prop_key, value) values ('fina2.security.passwordMinimalLen', '8');
insert into sys_properties (prop_key, value) values ('fina2.security.passwordWithNumsChars', '1');
insert into sys_properties (prop_key, value) values ('fina2.security.passwordValidityPeriod', '90');
insert into SYS_PROPERTIES (prop_key, value) values ('fina2.database.schemaVersion', '3.4.1');
insert into SYS_PROPERTIES (prop_key, value) values ('fina2.gui.version', '3.4.2c');
commit;